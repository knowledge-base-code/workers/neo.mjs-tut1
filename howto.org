#+title: Neo.mjs tutorial walkthrough
#+author: Xavier Brinon
#+date: [2021-10-07 Thu]
* Step 1: Building the SPA
** Creating the shell app
   ~npm init -y~ to start with a fresh config based on the git repo.
   ~npx neo-app~ will trigger a cli from where you can answer some basic questions.
   I'll answer by the default options because I have no imagination.
   #+begin_src shell
     haqadosch@haqadosch-ThinkPad:~/Documents/Dev/Javascript/Neo.mjs/neo.mjs-tut1$ npx neo-app
     Welcome to the neo.mjs app generator!
     current version of neo-app: 2.3.4
     ? Please choose a name for your neo workspace: workspace
     ? Please choose a name for your neo app: MyApp
     ? Please choose a theme for your neo app: all
     ? Please choose your main thread addons: DragDrop, Stylesheet
     ? Do you want to use SharedWorkers? Pick yes for multiple main threads (Browser Windows): no
   #+end_src

   1. The addons will be added later.
   2. As for the sharedWorkers, starting with only one thread will make debugging easier.
